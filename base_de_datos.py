#TAREA UNIDAD 3:
#EJERCICIO:
#Escriba un programa para construir un Sistema de Gestión de
#Estudiantes simple usando Python que pueda realizar las siguientes
#operaciones sobre la base de datos "academia" construida en las clases de la asignatura:
#Matricular estudiante
#Visualizar datos del estudiante
#Buscar estudiante por id
#Eliminar estudiante por id
#Colocar el enlace a su repositorio gitlab públic en donde
#cargue el código, imágenes de la ejecución, y otros archivos
#que considere importantes dentro de su programa.
#NOMBRE :VICTOR GUAMAN
import sqlite3


class BD:

    def __init__(self):
        self.conexion = sqlite3.connect('academia.sqlite')
        self.cursor = None
        print("Conexion establecida !!!")

    def crear_bd(self):
        sql_user = """CREATE TABLE User (
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, 
        name   TEXT UNIQUE, 
        email  TEXT)"""
        sql_course = """ CREATE TABLE Course (
        id      INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
        title   TEXT UNIQUE
        )"""
        sql_member = """ CREATE TABLE Member (
        user_id     INTEGER,
        course_id   INTEGER,
        role        INTEGER,
        PRIMARY KEY (user_id, course_id)
        ) """
        sql_borrar="""CREATE TABLE borrar(
        user_id   INTEGER,
        curse_id  INTEGER
        PRIMARY HEY(user_id,course_id"""

        self.cursor = self.conexion.cursor()
        self.cursor.execute(sql_user)
        self.cursor.execute(sql_course)
        self.cursor.execute(sql_member)
        self.cursor.execute(sql_borrar)   #mio
        print("Creadas las tablas !!!")
        self.cursor.close()

    def insertar_users(self):
        self.cursor = self.conexion.cursor()
        self.cursor.executemany("INSERT INTO User(name, email) VALUES(?, ?)",
                                [('Ingrid', 'ingrid.guaraca@unl.edu.ec'),
                                 ('Michael', 'michael.vasquez@unl.edu.ec'),
                                 ('Juan', 'juan.v.alvarez@unl.edu.ec')])
        self.conexion.commit()
        self.cursor.close()
        print ("Usuarios insertados")

    def insertar_courses(self):
        self.cursor = self.conexion.cursor()
        self.cursor.executemany("INSERT INTO Course(title) VALUES(?)",
                                [('Programación',),
                                 ('Evaluación de los Aprendizajes',),
                                 ('Educación No Formal',)])
        self.conexion.commit()
        self.cursor.close()
        print ("Cursos insertados")




    def insertar_members(self):
        self.cursor = self.conexion.cursor()
        try:
            self.cursor.executemany("INSERT INTO Member(user_id, course_id, role) VALUES(?, ?, ?)",
                                [(1, 1, 1),
                                 (2, 1, 0),
                                 (3, 1, 0),
                                 (1, 2, 0),
                                 (2, 2, 1),
                                 (2, 3, 1),
                                 (3, 3, 0)
                                ])
            self.conexion.commit()
            self.cursor.close()
            print("Miembros insertados")

        except sqlite3.Error as ex:
            print ("Error: ", ex)



#MATRICULAR ESTUDIANTE:

    def matricular_estudiante(self):
        self.cursor = self.conexion.cursor()
        try:
            self.cursor.executemany("INSERT INTO Member(user_id, course_id, role) VALUES(?, ?, ?)",
                                    [(1, 1, 1),
                                     (2, 1, 0),
                                     (3, 1, 0),
                                     (1, 2, 0),
                                     (2, 2, 1),
                                     (2, 3, 1),
                                     (3, 3, 0)
                                     ])
            self.conexion.commit()
            self.cursor.close()
            print("ESTUDIANTES MATRICULADOS")

        except sqlite3.Error as ex:
            print("Error: ", ex)

#CONSULTAR DATOS DE LOS ESTUDIANTES.

    def consultar_datos(self):
        sql = """SELECT User.name, Member.role, Course.title
                FROM User JOIN Member JOIN Course
                ON Member.user_id = User.id AND 
                Member.course_id = Course.id
                ORDER BY Course.title, Member.role DESC, User.name """
        self.cursor = self.conexion.cursor()
        self.cursor.execute(sql)
        filas = self.cursor.fetchall()
        for f in filas:
            print (f)
        self.cursor.close()
        print(filas)

#BUSCAR ESTUDIANTE POR ID.
    def buscar_estudiante(self):
         sql="SELEC * FROM member  WHERE user_id='1'"
         self.cursor.execute(sql)
         resultado=cursor.fetchall()
         print(resultado)

#BORRAR ESTUDIANTE POR ID.
    def borrar_estudiante(user_id):
        self.conexion = sqlite3.connect('academia.sqlite')
         self.cursor = self.conexion.cursor()
        self.cursor.executemany ("DELETE FROM Members WHERE user_id='1'"

         self.conexion.commit()
         self.cursor.close()


if __name__ == "__main__":
    base = BD()
    #base.crear_bd()
    #base.insertar_users()
    #base.insertar_members()
    #base.consultar_datos()
    #base.matricular estudiante
    #base.informacion de estudiante
    #basse.buscar estudiante por id
    #base.eliminar estudiante por id

